import image from './assets/images/48.jpg';
import appStyle from './App.module.css';
function App() {
  return (
    <div>
    <div className={appStyle.dcContainer}>
      <div>
        <img src={image} alt="avatar" className={appStyle.dcImage}></img>
      </div>
      <div>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div className={appStyle.dcInfo}>
        <b>Tammy Stevens</b> &nbsp; * &nbsp; Front End Developer
      </div>
    </div>
  </div>

  );
}

export default App;
